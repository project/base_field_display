<?php

namespace Drupal\base_field_display;

use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\path\Plugin\Field\FieldType\PathFieldItemList;

class AliasComputed extends PathFieldItemList {

  use ComputedItemListTrait;

  /**
   * Cached processed text.
   *
   * @var string|null
   */
  protected $processed = NULL;

  /**
   * {@inheritdoc}
   */
  public function computeValue() {
    $entity = $this->getEntity();
    if (!$entity->hasLinkTemplate('canonical') || $entity->isNew()) {
      return NULL;
    }

    $this->list[0] = $this->createItem(0, $entity->toUrl('canonical')->toString(TRUE)->getGeneratedUrl());
  }

}
