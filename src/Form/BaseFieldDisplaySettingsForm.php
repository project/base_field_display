<?php

namespace Drupal\base_field_display\Form;

use Drupal\Core\Cache\UseCacheBackendTrait;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure BaseField Display settings for this site.
 */
class BaseFieldDisplaySettingsForm extends ConfigFormBase {

  use UseCacheBackendTrait;

  /**
   * The settings name.
   *
   * @var string
   */
  const SETTINGS = 'base_field_display.settings';

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The entity display repository.
   *
   * @var EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);

    /** @var EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    $instance->setEntityTypeManager($entity_type_manager);

    /** @var EntityTypeBundleInfoInterface $entity_type_bundle_info */
    $entity_type_bundle_info = $container->get('entity_type.bundle.info');
    $instance->setEntityTypeBundleInfo($entity_type_bundle_info);

    /** @var EntityFieldManagerInterface $entity_field_manager */
    $entity_field_manager = $container->get('entity_field.manager');
    $instance->setEntityFieldManager($entity_field_manager);

    /** @var EntityDisplayRepositoryInterface $entityDisplayRepository */
    $entity_display_repository = $container->get('entity_display.repository');
    $instance->setEntityDisplayRepository($entity_display_repository);

    return $instance;
  }

  /**
   * Sets the entity type manager.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): void {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Sets the entity type bundle info.
   *
   * @param EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   */
  public function setEntityTypeBundleInfo(EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * Sets the entity field manager.
   *
   * @param EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function setEntityFieldManager(EntityFieldManagerInterface $entity_field_manager): void {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Sets the entity display repository.
   *
   * @param EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function setEntityDisplayRepository(EntityDisplayRepositoryInterface $entity_display_repository): void {
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'base_field_display_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [static::SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);

    $form['#title'] = $this->t('BaseField Display settings');
    $form['#tree'] = TRUE;

    $entity_types = $this->entityTypeManager->getDefinitions();
    usort($entity_types, function ($a, $b) {
      return strcmp($a->getLabel(), $b->getLabel());
    });

    $form['entities_tabs'] = [
      '#type' => 'vertical_tabs',
    ];

    foreach ($entity_types as $entity_type) {
      if (
        $entity_type->hasViewBuilderClass() &&
        $entity_type->entityClassImplements(FieldableEntityInterface::class)
      ) {

        $form['entities'][$entity_type->id()] = [
          '#type' => 'details',
          '#title' => $entity_type->getLabel(),
          '#group' => 'entities_tabs',
        ];

        $base_fields = $this->entityFieldManager->getBaseFieldDefinitions($entity_type->id());
        $options = [];
        foreach ($base_fields as $base_field) {
          $options[$base_field->getName()] = $this->t('@label (%name)', [
            '@label' => $base_field->getLabel(),
            '%name' => $base_field->getName(),
          ]);
        }
        $form['entities'][$entity_type->id()]['base_fields'] = [
          '#type' => 'checkboxes',
          '#options' => $options,
          '#default_value' => $config->get($entity_type->id()) ?? [],
          '#title' => $this->t('Base fields to activate for @label (%name) entity type', [
            '@label' => $entity_type->getLabel(),
            '%name' => $entity_type->id(),
          ]),
        ];

      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    foreach ($form_state->getValue('entities') as $entity_type_id => $entity_type_config) {
      $entity_base_fields = [];
      foreach ($entity_type_config['base_fields'] as $base_field => $activated) {
        if ($activated) {
          $entity_base_fields[] = $base_field;
        }
      }
      if ($entity_base_fields) {
        $config->set($entity_type_id, $entity_base_fields);
      }
      else {
        $config->clear($entity_type_id);
      }
    }
    $config->save();

    $this->entityFieldManager->clearCachedFieldDefinitions();

    parent::submitForm($form, $form_state);
  }

}
