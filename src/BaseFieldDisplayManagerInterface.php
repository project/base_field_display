<?php

namespace Drupal\base_field_display;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

interface BaseFieldDisplayManagerInterface {

  /**
   * Entity bundle field info alter hook.
   *
   * @param FieldDefinitionInterface[] $base_field_definitions
   *   The list of base field definitions for the entity type.
   * @param EntityTypeInterface $entity_type
   *   The entity type definition.
   *
   * @see \hook_entity_bundle_field_info_alter()
   */
  public function entityBundleFieldInfoAlter(array &$base_field_definitions, EntityTypeInterface $entity_type);

  /**
   * Entity type build hook.
   *
   * @param EntityTypeInterface[] $entity_types
   *   An associative array of all entity type definitions, keyed by the entity
   *   type name. Passed by reference.
   *
   * @see \hook_entity_type_build()
   */
  public function entityTypeBuild(array &$entity_types);

  /**
   * Entity (node, taxonomy_term...) preprocess hook.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param array $variables
   *   The variables array (modify in place).
   *
   * @see \hook_preprocess_HOOK()
   */
  public function preprocessEntity(string $entity_type_id, array &$variables);

}
