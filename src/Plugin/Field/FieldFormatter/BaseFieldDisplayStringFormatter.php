<?php

namespace Drupal\base_field_display\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;

/**
 * Plugin implementation of the 'plain_text' formatter.
 *
 * @FieldFormatter(
 *   id = "base_field_display_string",
 *   label = @Translation("Plain text"),
 *   field_types = {
 *     "uuid",
 *     "password",
 *   }
 * )
 */
class BaseFieldDisplayStringFormatter extends StringFormatter {
}
