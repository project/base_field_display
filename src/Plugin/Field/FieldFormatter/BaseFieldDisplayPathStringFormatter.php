<?php

namespace Drupal\base_field_display\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\path\Plugin\Field\FieldType\PathItem;

/**
 * Plugin implementation of the 'path_string' formatter.
 *
 * @FieldFormatter(
 *   id = "base_field_display_path_string",
 *   label = @Translation("String"),
 *   field_types = {
 *     "path",
 *   }
 * )
 */
class BaseFieldDisplayPathStringFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    /** @var PathItem $item */
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => $item->getValue()['alias'] ?? '',
      ];
    }

    return $elements;
  }

}
