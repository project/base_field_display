<?php

namespace Drupal\base_field_display;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class BaseFieldDisplayManager implements BaseFieldDisplayManagerInterface {

  use StringTranslationTrait;

  /**
   * The settings name.
   *
   * @var string
   */
  const SETTINGS = 'base_field_display.settings';

  /**
   * The module config.
   *
   * @var ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Construct a new BaseFieldDisplayManager object.
   *
   * @param ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get(static::SETTINGS);
  }

  /**
   * {@inheritdoc}
   */
  public function entityBundleFieldInfoAlter(array &$base_field_definitions, EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('canonical')) {
      $base_field_definitions['base_field_display_alias'] = BaseFieldDefinition::create('path')
        ->setTargetEntityTypeId($entity_type->id())
        ->setName('base_field_display_alias')
        ->setCardinality(1)
        ->setLabel($this->t('Alias'))
        ->setComputed(TRUE)
        ->setTranslatable(TRUE)
        ->setClass('\Drupal\base_field_display\AliasComputed');
    }

    if ($entity_config = $this->config->get($entity_type->id())) {
      foreach ($base_field_definitions as $base_field_name => $base_field_definition) {
        if (in_array($base_field_name, $entity_config)) {
          $base_field_definitions[$base_field_name] = $base_field_definition->setDisplayConfigurable('view', TRUE);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeBuild(array &$entity_types) {
    // Allow skipping of extra preprocessing for configurable display.
    foreach (['node', 'taxonomy_term'] as $entity_type_id) {
      if (!empty($entity_types[$entity_type_id])) {
        $entity_types[$entity_type_id]->set('enable_base_field_custom_preprocess_skipping', TRUE);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preprocessEntity(string $entity_type_id, array &$variables) {
    if ($entity_config = $this->config->get($entity_type_id)) {
      $name_field = $entity_type_id == 'node' ? 'title' : 'name';
      if (in_array($name_field, $entity_config)) {
        // Allow title to be rendered again as field even if displayed in h1.
        $variables['content'][$name_field]['#printed'] = FALSE;
        $variables['content'][$name_field]['#is_page_title'] = FALSE;
      }
    }
  }

}
