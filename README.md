# BaseField Display

This module does a very simple thing: it exposes entity base fields in view modes.

It can expose base fields for any core or custom content entities, like Content (node), Media, File, Paragraph, Taxonomy term, User...

Example of Base fields provided by node entity type:

- ID (nid)
- UUID (uuid)
- Content Type (type)
- Title (title)
- Authored by (uid)
- Authored on (created)
- ...

This module adds them in view modes with their original available field formatters depending on their type. It also provides very simple string formatters for some core field types that did not have any (uuid, password and path).

## Configuration

After installing the module as usual, you need to activate some base fields in Configuration > Content authoring > BaseField Display.
